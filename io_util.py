import os

from bs4 import dammit


def read_html_file(path: str) -> str:
    with open(path, 'rb') as f:
        data_bin = f.read()
        return dammit.UnicodeDammit(data_bin, is_html=True).unicode_markup


def read_text_file(folder: str, name: str) -> str:
    with open(os.path.join(folder, name), 'r', encoding='utf-8') as f:
        return f.read()


def save_to_file(path: str, data: str):
    folder = os.path.dirname(path)
    if not os.path.exists(folder):
        os.makedirs(folder)

    with open(path, 'w', encoding='utf-8') as f:
        f.write(data)
