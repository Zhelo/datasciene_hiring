import glob
import os
import re
import sys

from bs4 import BeautifulSoup

from io_util import read_html_file, save_to_file


def __make_soup(data: str) -> BeautifulSoup:
    return BeautifulSoup(data, features='lxml')


def __remove_basic(soup):
    remove_tags = ['header', 'footer', 'nav', 'menu', 'script', 'noscript']
    style_hidden = re.compile('(visibility:[\\s]*hidden)|(display:[\\s]*none)')

    for tag in remove_tags:
        for element in soup.find_all(tag):
            element.decompose()
    for element in soup.find_all(style=style_hidden):
        element.decompose()


def parse_html_baseline(data: str) -> str:
    """Removes only the header/footer, navigation, and hidden elements"""
    soup = __make_soup(data)
    __remove_basic(soup)

    return soup.get_text(' ', strip=True)


def parse_html_tuned(data: str) -> str:
    """Same as parse_html_baseline, but tries to remove more elements marked as menus/sidebars/headers/footers
    by inspecting class and id values"""
    remove_class_contains = ['menu', 'sidebar', 'header', 'footer', 'breadcrumb', 'banner', 'subnav']
    remove_id_contains = remove_class_contains

    soup = __make_soup(data)

    __remove_basic(soup)

    for class_ in remove_class_contains:
        for element in soup.select('[class*="{0}"]'.format(class_)):
            if element.name != 'body' and element.name != 'main':  # don't drop too much
                element.decompose()

    for id_ in remove_id_contains:
        for element in soup.select('[id*="{0}"]'.format(id_)):
            if element.name != 'body' and element.name != 'main':  # don't drop too much
                element.decompose()

    return soup.get_text(' ', strip=True)


def process_html_files(html_folder: str) -> None:
    for file in glob.glob(os.path.join(html_folder, '*.html')):
        path = os.path.abspath(file)
        try:
            data = read_html_file(path)
        except Exception as e:
            print('{0}: failed to read, error: {1}'.format(path, e))
            continue

        text_file_name = os.path.splitext(os.path.basename(file))[0] + '.txt'
        save_to_file(os.path.join('text_tuned', text_file_name), parse_html_tuned(data))
        save_to_file(os.path.join('text_baseline', text_file_name), parse_html_baseline(data))


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Expected a path to html directory as 1st argument')
        sys.exit(-1)

    process_html_files(sys.argv[1])
